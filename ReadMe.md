# Assignment 2,

## Employee Management Application

### How it works

This is a Spring Boot App where we simulate the usage of controller methods with the use of our old friend Postman
application.

We have a base URL which is http://localhost:8080/employees/

#### GET Request the full list of the employees:

http://localhost:8080/employees
<br>Shows the current employees working in this corporation.

#### GET Request a specific employee by its id:

http://localhost:8080/employees/(id)
<br>Gets the specific employee from the id number.

#### GET Request a list of the sum of devices each employee have:

http://localhost:8080/employees/devices/summary
<br>Get you a list of how many devices each employee have in their possession.

#### GET Request a list of "new" employees who have been employed in the last two years:

http://localhost:8080/employees/new 
<br>Get you a list of employees that have been hired in the last two years with their employee code.

#### GET Request a list of Fulltime employees, their birthday and how many days left until next birthday:

http://localhost:8080/employees/birthdays
<br>Get you a list of fulltime working employees, their birthdays and a counter of how many days left until their next
birthday.

#### POST Create a new employee:

http://localhost:8080/employees
<br>Creates a new employee but you must follow the template otherwise you will get an exception.

#### Here is an example of the acceptable JSON body:

```
{
"id": 2,
"firstName": "Jane",
"lastName": "Doe",
"dateOfBirth": "17/05/1984",
"employementType": "Fulltime",
"employmentDate": "1/12/2018",
"position": "Manager",
"employeeCode": "1634751505",
"devices": [
"Alienware X17",
"Samsung 30-inch LED Monitor"
]
}
```

#### PUT Replace a employee:

http://localhost:8080/employees/(id)
<br>Targets the specified id that u put in the url and replaces it if you followed the template otherwise an exception will
be made.

#### PATCH Change properties of an employee:

http://localhost:8080/employees/(id)
<br>Targets the specified id that u put in the url and replaces it if you followed the template and use a (id) that is in
use, otherwise an exception will be made.

#### DELETE Delete employee:

http://localhost:8080/employees/(id)
<br>Deletes the targeted employee with the use of your (id) input in the url if it exists.

### About

This is our second bigger assignment in which we were asked to create an Employee API, being able to track employees,
their devices, new employees, their birthdays, and change/replace/delete properties and whole employees.

As part of our assignment we had an optional task in which we had to make to be able to call ourself real web
developers, we had to create a ci/cd pipeline in gitlab that builds a docker image and pushes the API to the web whicha
can be seen at this URL: https://employee-management-appli.herokuapp.com/employees

//Jerry Lindström