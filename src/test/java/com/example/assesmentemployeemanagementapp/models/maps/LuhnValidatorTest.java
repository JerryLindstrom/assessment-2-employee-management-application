package com.example.assesmentemployeemanagementapp.models.maps;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnValidatorTest {

    @BeforeEach
    void init() {
        LuhnValidator luhnValidator = new LuhnValidator();
    }

    @Test
    public void testValidLuhnValue_expectedReturnTrue() {
        //Arrange
        String luhnNumber = "1507119608";
        boolean expected = true;

        //Act
        boolean actual = LuhnValidator.evaluateLuhnValue(luhnNumber);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testInvalidLuhnValue_expectedReturnFalse() {
        //Arrange
        String invalidLuhnNumber = "1324567415";
        boolean expected = false;

        //Act
        boolean actual = LuhnValidator.evaluateLuhnValue(invalidLuhnNumber);

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    public void testSixDigitCombo_expectedReturnFalse() {
        //Arrange
        String invalidLuhnNumber = "245674";
        boolean expected = false;

        //Act
        boolean actual = LuhnValidator.evaluateLuhnValue(invalidLuhnNumber);

        //Assert
        assertEquals(expected, actual);

    }

    @Test
    public void testTwelveDigitCombo_expectedReturnFalse() {
        //Arrange
        String invalidLuhnNumber = "150711960815";
        boolean expected = false;

        //Act
        boolean actual = LuhnValidator.evaluateLuhnValue(invalidLuhnNumber);

        //Assert
        assertEquals(expected, actual);

    }
}