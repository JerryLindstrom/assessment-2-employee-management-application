package com.example.assesmentemployeemanagementapp.dataaccess;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.domain.EmployementType;
import com.example.assesmentemployeemanagementapp.models.domain.Position;

import com.example.assesmentemployeemanagementapp.models.dto.EmployeeDevicesDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {
    EmployeeRepository employeeRepository;

    @BeforeEach
    void init() {
        employeeRepository = new EmployeeRepository();
    }

    @Test
    void getAllEmployees_initialCollectionExists_shouldReturnThreeEmployees() {
        //arrange
        int totalExpectedEmployees = 3;

        //act
        int actualTotalEmployees = employeeRepository.getAllEmployees().size();

        //assert
        assertEquals(totalExpectedEmployees, actualTotalEmployees);
    }

    @Test
    void getEmployee_employeeWithIdExist_returnEmployeeWithId() {
        Employee expectedEmployee = new Employee(1, "John", "Doe", "14/02/1980",
                EmployementType.Parttime, "31/3/2021", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{"Dell XPS 13"})));
        int testId = 1;

        Employee actualEmployee = employeeRepository.getEmployee(testId);

        assertEquals(expectedEmployee, actualEmployee);
    }

    @Test
    void getEmployee_employeeWithIdDoesNotExist_returnNull(){
        int testId = 4;

        Employee employee = employeeRepository.getEmployee(testId);

        assertNull(employee);
    }

    @Test
    void createEmployee_validEmployee_returnFourEmployees() {
        Employee employeeToAdd = new Employee(4, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{""})));
        int expectedTotalEmployees = 4;

        employeeRepository.createEmployee(employeeToAdd);
        int actualTotalEmployees = employeeRepository.getAllEmployees().size();

        assertEquals(expectedTotalEmployees, actualTotalEmployees);
    }

    @Test
    void createEmployee_validEmployee_returnEmployeeDetails() {
        Employee employeeToAdd = new Employee(4, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{""})));

        Employee actualEmployee = employeeRepository.createEmployee(employeeToAdd);

        assertEquals(employeeToAdd, actualEmployee);
    }

    @Test
    void createEmployee_nullEmployee_throwNullException() {
        assertThrows(NullPointerException.class, () -> employeeRepository.createEmployee(null));
    }

    @Test
    void replaceEmployee_updateExistingValidEmployee_returnEmployeeDetails() {
        Employee employeeToReplace = new Employee(2, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{""})));

        Employee actualEmployee = employeeRepository.replaceEmployee(employeeToReplace);

        assertEquals(employeeToReplace, actualEmployee);
    }

    @Test
    void modifyEmployee_modifyExistingValidEmployee_returnEmployeeDetails() {
        Employee employeeToModify = new Employee(2, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{""})));

        Employee actualEmployee = employeeRepository.modifyEmployee(employeeToModify);

        assertEquals(employeeToModify, actualEmployee);
    }

    @Test
    void deleteEmployee_deletesEmployeeWithIdTwo_returnSizeOfListEqualsTwo() {
        int testId = 2;
        int expected = 2;

        employeeRepository.deleteEmployee(testId);
        int actual = employeeRepository.getAllEmployees().size();

        assertEquals(expected, actual);
    }

    @Test
    void isValidEmployeeInput_validEmployeeInput_returnTrue() {
        Employee employeeToTryValidation = new Employee(4, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "7743305539",
                new ArrayList<>(List.of(new String[]{""})));

        boolean actualEmployee = employeeRepository.isValidEmployeeInput(employeeToTryValidation);
        boolean expected = true;
        assertEquals(actualEmployee, expected);
    }

    @Test
    void employeeExists_validIdInput_returnTrue() {
        int testId = 2;
        boolean expected = true;

        boolean actual = employeeRepository.employeeExists(testId);

        assertEquals(actual, expected);
    }

    @Test
    void employeeExists_invalidIdInput_returnFalse() {
        int testId = 5;
        boolean expected = false;

        boolean actual = employeeRepository.employeeExists(testId);

        assertEquals(actual, expected);
    }

    @Test
    void getDeviceTotals_initializedDeviceData_firstDeviceEqualsOne() {
        String expectedCount = "John Doe has 1 devices.";

        EmployeeDevicesDto results = employeeRepository.getDeviceTotals();
        assertEquals(expectedCount, results.getDeviceList().get(0));
    }

    @Test
    void getDeviceTotals_addNewDeviceFromFirstDeviceList_firstDeviceEqualsTwo() {
        String expectedCount = "Eric Johnson has 2 devices.";

        Employee employeeToAdd = new Employee(4, "Eric", "Johnson", "22/08/1964",
                EmployementType.Fulltime, "21/2/2022", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{"Dell XPS 13", "Alienware X17"})));

        employeeRepository.createEmployee(employeeToAdd);
        EmployeeDevicesDto results = employeeRepository.getDeviceTotals();
        assertEquals(expectedCount, results.getDeviceList().get(3));
    }

    @Test
    void testEmployeeCode_addingNewUniqueEmployeeCodeThatAlreadyExists_shouldReturnFalse() {
        boolean expected = false;

        boolean actual = employeeRepository.employeeCodeAlreadyExists("2018269734");

        assertEquals(expected, actual);
    }

    @Test
    void testEmployeeCode_addingNewUniqueEmployeeCode_shouldReturnTrue() {
        boolean expected = true;

        boolean actual = employeeRepository.employeeCodeAlreadyExists("8256396337");

        assertEquals(expected, actual);
    }
}