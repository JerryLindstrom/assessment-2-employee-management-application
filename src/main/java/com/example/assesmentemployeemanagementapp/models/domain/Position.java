package com.example.assesmentemployeemanagementapp.models.domain;

public enum Position {
    Manager,
    Developer,
    Tester
}
