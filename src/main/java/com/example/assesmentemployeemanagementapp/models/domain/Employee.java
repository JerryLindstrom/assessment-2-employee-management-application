package com.example.assesmentemployeemanagementapp.models.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@Getter
@Setter
@Data
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private EmployementType employementType;
    private String employmentDate;
    private Position position;
    private String employeeCode;
    private ArrayList<String> devices;
}
