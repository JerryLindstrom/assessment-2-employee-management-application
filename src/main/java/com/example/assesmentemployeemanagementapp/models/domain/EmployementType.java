package com.example.assesmentemployeemanagementapp.models.domain;

public enum EmployementType {
    Fulltime,
    Parttime
}
