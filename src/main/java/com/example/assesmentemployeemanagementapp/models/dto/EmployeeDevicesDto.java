package com.example.assesmentemployeemanagementapp.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;


@AllArgsConstructor
@Getter
@Setter
public class EmployeeDevicesDto {
    private ArrayList<String> deviceList;
}
