package com.example.assesmentemployeemanagementapp.models.maps;

import org.springframework.stereotype.Component;

@Component
public class LuhnValidator {
    /*This method takes in your employement code as a string and first checks if the length of the string
    * equals to 10 digits, then we start the check by evaluating each digit from the last digit and working our way to
    * the first digit, we alternate, so it multiplies every other digit by two to follow the luhn algorithm rule.
    * We add all digits together and the sum in the end should be "summable" by 10 to be a valid Luhn number and return true.*/

    public static boolean evaluateLuhnValue(String evaluateNumber) {

        if (evaluateNumber.length() !=10) {
            return false;
        }

        int result = 0;
        int numbers = evaluateNumber.length();
        boolean alternateOddEvenNum = false;

        for (int i = numbers - 1; i >= 0; i--) {
            int digit = evaluateNumber.charAt(i) - '0';

            if (alternateOddEvenNum) {
                digit = digit * 2;
            }
            result += digit / 10;
            result += digit % 10;

            alternateOddEvenNum = !alternateOddEvenNum;
        }
        return (result % 10 == 0);
    }
}
