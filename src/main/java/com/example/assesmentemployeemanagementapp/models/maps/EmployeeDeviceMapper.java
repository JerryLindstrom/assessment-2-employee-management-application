package com.example.assesmentemployeemanagementapp.models.maps;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeDevicesDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
/*Mapper that takes in a Map and creates an empty ArrayList,
* we loop through the "value" part of the map and saves in this case the firstname, lastname and count how
* many devices that employee has and stores it in separate variables.
* We then add those after each iteration in to the newly created arraylist and iterate through the map until "value" part
* is at the end and return our new arraylist to the deviceDto.*/

@Component
public class EmployeeDeviceMapper {

    public static EmployeeDevicesDto mapEmployeeDevicesDto(Map<Integer, Employee> employees) {
        ArrayList<String> deviceMapper = new ArrayList<>();
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            String firstName = entry.getValue().getFirstName();
            String lastName = entry.getValue().getLastName();
            int devices = entry.getValue().getDevices().size();

            deviceMapper.add(firstName + " " + lastName + " has " + devices + " devices.");
        }
        return new EmployeeDevicesDto(deviceMapper);
    }
}