package com.example.assesmentemployeemanagementapp.models.maps;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeNewWithinTwoYearsDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;


@Component
public class NewEmployeeMapper {
    /*This method takes in the employement date as a string and formats it to requested formatting,
     * and checks if that date is less than two years ago.*/
    private static Boolean checkIfEmployedLessThanTwo(String employementTime) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        LocalDate today = LocalDate.now();
        LocalDate twoYearsAgo = today.minusYears(2);
        LocalDate dateToCheck = LocalDate.parse(employementTime, formatter);

        return dateToCheck.isAfter(twoYearsAgo);
    }
    /*Mapper that takes in a Map and creates an empty ArrayList,
     * we loop through the "value" part of the map and saves in this case the firstname, lastname and
     * the employee code and stores it in separate variables.
     * We then add those after each iteration in to the newly created arraylist and iterate through the map until "value" part
     * is at the end.
     * We then call for our method that checks if our employee have been employeed for less than two years
     * and return our new arraylist to the newWithinTwoYearsDto.*/
    public static EmployeeNewWithinTwoYearsDto mapNewEmployees(Map<Integer, Employee> employees) {

        ArrayList<String> twoYearEmployeeMapper = new ArrayList<>();
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            String employmentTime = entry.getValue().getEmploymentDate();
            String firstName = entry.getValue().getFirstName();
            String lastName = entry.getValue().getLastName();
            String code = entry.getValue().getEmployeeCode();

            if (checkIfEmployedLessThanTwo(employmentTime)) {
                twoYearEmployeeMapper.add("Name: " + firstName + " " + lastName);
                twoYearEmployeeMapper.add("Employee code: " + code);
            }
        }
        return new EmployeeNewWithinTwoYearsDto(twoYearEmployeeMapper);
    }
}
