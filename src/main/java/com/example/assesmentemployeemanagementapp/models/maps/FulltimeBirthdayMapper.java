package com.example.assesmentemployeemanagementapp.models.maps;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.domain.EmployementType;
import com.example.assesmentemployeemanagementapp.models.dto.FulltimeEmployeeBirthdaysDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Map;

@Component
public class FulltimeBirthdayMapper {
        /*This method takes in the birthday as a string and formats it to requested formatting,
        * if your birthday has already been this year it will add one year to your next birthday and
        * count the days between today and your next birthday and return that number of days.*/
    private static String daysRemainingUntilParty(String birthday) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate today = LocalDate.now();
        LocalDate dateOfBirth = LocalDate.parse(birthday, formatter);
        LocalDate nextBirthday = dateOfBirth.withYear(today.getYear());

        if (nextBirthday.isEqual(today)) {
            return "Your birthday is today! Happy Birthday";
        }
        if (nextBirthday.isBefore(today) || nextBirthday.isEqual(today)) {
            nextBirthday =nextBirthday.plusYears(1);
        }

        long daysUntilNext =  ChronoUnit.DAYS.between(today,nextBirthday);
        return String.valueOf(daysUntilNext);
    }
    /*Mapper that takes in a Map and creates an empty ArrayList,
     * we loop through the "value" part of the map and saves in this case the employement status if it equals to
     * fulltime employee, firstname, lastname, birthday and stores it in separate variables.
     * We then add those after each iteration in to the newly created arraylist and iterate through the map until "value" part
     * is at the end.
     * We then call for our days until next birthday method that calculates the days remaining until next birthday
     * and return our new arraylist to the birthdaydto.*/
    public static FulltimeEmployeeBirthdaysDto mapFulltimeEmployeeBirthdays(Map<Integer, Employee> employees) {

        ArrayList<String> twoYearEmployeeMapper = new ArrayList<>();
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            boolean fulltimeEmployee = entry.getValue().getEmployementType().equals(EmployementType.Fulltime);
            String birthday = entry.getValue().getDateOfBirth();
            String firstName = entry.getValue().getFirstName();
            String lastName = entry.getValue().getLastName();

            if (fulltimeEmployee) {
                twoYearEmployeeMapper.add("Name: " + firstName + " " + lastName);
                twoYearEmployeeMapper.add("Birthdate: " +birthday);
                twoYearEmployeeMapper.add("Days until next birthday: " + daysRemainingUntilParty(birthday));
            }
        }
        return new FulltimeEmployeeBirthdaysDto(twoYearEmployeeMapper);
    }
}
