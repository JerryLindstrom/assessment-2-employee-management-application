
package com.example.assesmentemployeemanagementapp.controllers;

import com.example.assesmentemployeemanagementapp.dataaccess.EmployeeRepository;
import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeDevicesDto;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeNewWithinTwoYearsDto;
import com.example.assesmentemployeemanagementapp.models.dto.FulltimeEmployeeBirthdaysDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController   //Set base URL for the controller
@RequestMapping(value = "/employees")
public class EmployeeController {
    private EmployeeRepository employeeRepository;

   @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {this.employeeRepository = employeeRepository;}

    //Get all the employees
    @GetMapping
    public ResponseEntity<HashMap<Integer, Employee>> getAllEmployees() {
       if (employeeRepository.getAllEmployees().isEmpty()) {
           return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
       }
        return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.OK);
    }

    //Get a specific employee by its id
    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.OK);
    }

    //Gets the number of devices per employee as a list
    @GetMapping("/devices/summary")
    public ResponseEntity<EmployeeDevicesDto> deviceList() {
        return new ResponseEntity<>(employeeRepository.getDeviceTotals(), HttpStatus.OK);
    }

    //Gets a list of the "newly"(last 2 years) employed employees
    @GetMapping("/new")
    public ResponseEntity<EmployeeNewWithinTwoYearsDto> twoYearList() {
        return new ResponseEntity<>(employeeRepository.getListOfNewEmployeeWithinTwoYears(), HttpStatus.OK);
    }

    //Gets a list of the fulltime employees, their birthdays and how many days left until next birthday
    @GetMapping("/birthdays")
    public ResponseEntity<FulltimeEmployeeBirthdaysDto> fulltimeEmployeeBirthdays() {
       return new ResponseEntity<>(employeeRepository.getListOfFulltimeEmployeesBirthdays(), HttpStatus.OK);
    }

    //Creates a new employee if you follow the template for an employee, id does not already exist and
    // the employee code is a valid luhn number
    @PostMapping
    public ResponseEntity<Employee> createNewEmployee(@RequestBody Employee employee) {
        if (!employeeRepository.isValidEmployeeInput(employee)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.employeeCodeAlreadyExists(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (employeeRepository.employeeExists(employee.getId())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.isValidLuhnNumber(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(employeeRepository.createEmployee(employee), HttpStatus.CREATED);
    }

    //Replaces targeted id employee if id that exists is provided, you follow the valid employee template and the
    //employee code is a valid luhn number
    @PutMapping("{id}")
    public ResponseEntity<Employee> replaceEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!employeeRepository.isValidEmployeeInput(employee, id)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.employeeCodeAlreadyExists(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!employeeRepository.isValidLuhnNumber(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        employeeRepository.replaceEmployee(employee);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    //Patches(updates) the targeted id employee if id that exists is provided, the employee code is a valid
    // luhn number
    @PatchMapping("/{id}")
    public ResponseEntity<Employee> modifyEmployee(@PathVariable int id, @RequestBody Employee employee) {
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if (!employeeRepository.employeeCodeAlreadyExists(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!employeeRepository.isValidLuhnNumber(employee.getEmployeeCode())) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
         employeeRepository.modifyEmployee(employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    //Deletes targeted id employee if id that exists is provided
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable int id) {
        if (!employeeRepository.employeeExists(id)) {
            return new ResponseEntity<>("Employee with id " + id + " does not exist!", HttpStatus.NOT_FOUND);
        }
        employeeRepository.deleteEmployee(id);
        return new ResponseEntity<>("Employee with id " + id + " has been deleted!", HttpStatus.OK);
    }

}
