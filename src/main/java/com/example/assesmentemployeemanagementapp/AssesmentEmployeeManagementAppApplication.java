package com.example.assesmentemployeemanagementapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssesmentEmployeeManagementAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssesmentEmployeeManagementAppApplication.class, args);
    }

}
