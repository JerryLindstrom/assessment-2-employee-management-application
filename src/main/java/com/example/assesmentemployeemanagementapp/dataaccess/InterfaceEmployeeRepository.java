package com.example.assesmentemployeemanagementapp.dataaccess;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeDevicesDto;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeNewWithinTwoYearsDto;
import com.example.assesmentemployeemanagementapp.models.dto.FulltimeEmployeeBirthdaysDto;

import java.util.HashMap;

public interface InterfaceEmployeeRepository {
    HashMap<Integer, Employee> getAllEmployees();
    Employee getEmployee(int id);
    Employee createEmployee(Employee employee);
    Employee replaceEmployee(Employee employee);
    Employee modifyEmployee(Employee employee);
    void deleteEmployee(int id);
    boolean employeeExists(int id);
    boolean isValidLuhnNumber(String employeeCode);
    boolean employeeCodeAlreadyExists(String employeeCode);
    EmployeeDevicesDto getDeviceTotals();
    EmployeeNewWithinTwoYearsDto getListOfNewEmployeeWithinTwoYears();
    FulltimeEmployeeBirthdaysDto getListOfFulltimeEmployeesBirthdays();
}
