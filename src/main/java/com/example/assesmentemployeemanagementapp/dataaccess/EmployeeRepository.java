package com.example.assesmentemployeemanagementapp.dataaccess;

import com.example.assesmentemployeemanagementapp.models.domain.Employee;
import com.example.assesmentemployeemanagementapp.models.domain.EmployementType;
import com.example.assesmentemployeemanagementapp.models.domain.Position;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeDevicesDto;
import com.example.assesmentemployeemanagementapp.models.dto.EmployeeNewWithinTwoYearsDto;
import com.example.assesmentemployeemanagementapp.models.dto.FulltimeEmployeeBirthdaysDto;
import com.example.assesmentemployeemanagementapp.models.maps.EmployeeDeviceMapper;
import com.example.assesmentemployeemanagementapp.models.maps.FulltimeBirthdayMapper;
import com.example.assesmentemployeemanagementapp.models.maps.LuhnValidator;
import com.example.assesmentemployeemanagementapp.models.maps.NewEmployeeMapper;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class EmployeeRepository implements InterfaceEmployeeRepository {
    private HashMap<Integer, Employee> employees = initialEmployeeList();

    //Creates a in memory "database" with initial employees
    private HashMap<Integer, Employee> initialEmployeeList() {
        var employeeInitialList = new HashMap<Integer, Employee>();

        employeeInitialList.put(1, new Employee(1, "John", "Doe", "14/02/1980",
                EmployementType.Parttime, "31/3/2021", Position.Tester, "2018269734",
                new ArrayList<>(List.of(new String[]{"Dell XPS 13"}))));
        employeeInitialList.put(2, new Employee(2, "Jane", "Doe", "17/05/1984",
                EmployementType.Fulltime, "1/12/2018", Position.Manager, "1634751505",
                new ArrayList<>(List.of(new String[]{"Alienware X17", "Samsung 30-inch LED Monitor"}))));
        employeeInitialList.put(3, new Employee(3, "Sven", "Svensson", "22/03/1987",
                EmployementType.Fulltime, "15/2/2019", Position.Developer, "1507119608",
                new ArrayList<>(List.of(new String[]{"Dell XPS 15", "Samsung 27-inch LED Monitor"}))));

        return employeeInitialList;
    }

    //Returns the complete list of employees
    @Override
    public HashMap<Integer, Employee> getAllEmployees() {
        return employees;
    }

    //Returns specific employee requested by its id
    @Override
    public Employee getEmployee(int id) {
        return employees.get(id);
    }

    //Creates a new employee with specified id
    @Override
    public Employee createEmployee(Employee employee) {
        employees.put(employee.getId(), employee);
        return employees.get(employee.getId());
    }

    //Replaces targeted employee by its id
    @Override
    public Employee replaceEmployee(Employee employee) {
        var employeeToDelete = getEmployee(employee.getId());
        employees.remove(employeeToDelete);

        employees.put(employee.getId(), employee);
        return getEmployee(employee.getId());
    }

    //Modifies an employee targeted by its id if it exists
    @Override
    public Employee modifyEmployee(Employee employee) {
        var employeeToModify = getEmployee(employee.getId());

        employeeToModify.setFirstName(employee.getFirstName());
        employeeToModify.setLastName(employee.getLastName());
        employeeToModify.setDateOfBirth(employee.getDateOfBirth());
        employeeToModify.setEmploymentDate(employee.getEmploymentDate());
        employeeToModify.setEmployeeCode(employee.getEmployeeCode());
        employeeToModify.setEmployementType(employee.getEmployementType());
        employeeToModify.setPosition(employee.getPosition());
        employeeToModify.setDevices(employee.getDevices());

        return employeeToModify;
    }

    //Deletes the employee targeted by id if it exists
    @Override
    public void deleteEmployee(int id) {
        employees.remove(id);
    }

    //Checks if the employee input is valid or not by the requested format in the post request
    public boolean isValidEmployeeInput(Employee employee) {
        return employee.getId() > 0 && employee.getFirstName() != null && employee.getLastName() != null
                && employee.getPosition() != null && employee.getDateOfBirth() != null && employee.getEmploymentDate() != null
                && employee.getEmployeeCode() != null && employee.getDevices() != null;
    }

    //Checks if the employee input is valid by the requested format and id in the put request
    public boolean isValidEmployeeInput(Employee employee, int id) {
        return isValidEmployeeInput(employee) && employee.getId() == id;
    }

    //Checks if the employeecode is a valid luhn number
    @Override
    public boolean isValidLuhnNumber(String employeeCode) {
        return LuhnValidator.evaluateLuhnValue(employeeCode);
    }

    //Checks if the employee exists
    @Override
    public boolean employeeExists(int id) {
        return employees.containsKey(id);
    }

    //Calls the method mapemployeedevicesdto in employeedevicemapper and feeds in employees
    //Gets the response as list as specified in the dto
    @Override
    public EmployeeDevicesDto getDeviceTotals() {
        return EmployeeDeviceMapper.mapEmployeeDevicesDto(employees);
    }

    //Gets a list of the "newly"(last 2 years) employed employees
    @Override
    public EmployeeNewWithinTwoYearsDto getListOfNewEmployeeWithinTwoYears() {
        return NewEmployeeMapper.mapNewEmployees(employees);
    }

    //Gets a list of the fulltime employees, their birthdays and how many days left until next birthday
    @Override
    public FulltimeEmployeeBirthdaysDto getListOfFulltimeEmployeesBirthdays() {
        return FulltimeBirthdayMapper.mapFulltimeEmployeeBirthdays(employees);
    }

    //If you create/replace employee this method will run and check that you don't put in duplicate employee code.
    @Override
    public boolean employeeCodeAlreadyExists(String employeeCode) {
        for (Map.Entry<Integer, Employee> employee : employees.entrySet()) {
            if (employee.getValue().getEmployeeCode().equals(employeeCode)){
                return false;
            }
        }
        return true;
    }


}
